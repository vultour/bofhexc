package bofhexc

import (
	"math/rand"
	"reflect"
	"testing"
)

func TestMax(t *testing.T) {
	if Max() < 1 {
		t.Error("No excuses found")
	}
	t.Log("Excuses:", Max())
}

func TestDel(t *testing.T) {
	var excuse string
	var err error
	var mock []string = []string{
		"one",
		"two",
		"three",
		"four",
		"five",
	}

	mock, excuse, err = del(mock, 0)
	if err != nil {
		t.Error(err)
	}
	if excuse != "one" {
		t.Error("Invalid return value:", excuse)
	}
	if !reflect.DeepEqual(mock, []string{"two", "three", "four", "five"}) {
		t.Error("Slice consistency failure")
	}

	mock, excuse, err = del(mock, 3)
	if err != nil {
		t.Error(err)
	}
	if excuse != "five" {
		t.Error("Invalid return value:", excuse)
	}
	if !reflect.DeepEqual(mock, []string{"two", "three", "four"}) {
		t.Error("Slice consistency failure")
	}

	mock, excuse, err = del(mock, 1)
	if err != nil {
		t.Error(err)
	}
	if excuse != "three" {
		t.Error("Invalid return value:", excuse)
	}
	if !reflect.DeepEqual(mock, []string{"two", "four"}) {
		t.Error("Slice consistency failure")
	}

	mock, excuse, err = del(mock, 2)
	if err == nil {
		t.Error("No error when expected for value 2")
	}

	mock, excuse, err = del(mock, -1)
	if err == nil {
		t.Error("No error when expected for value -1")
	}
}

func TestOne(t *testing.T) {
	var excuse string = One()
	if len([]rune(excuse)) < 1 {
		t.Error("Zero length excuse")
	}
}

func TestMultiple(t *testing.T) {
	num := rand.Intn(Max()) + 1
	exc, err := Multiple(num)

	if err != nil {
		t.Errorf("Multiple failed (%v): %v", num, err)
	}
	if len(exc) != num {
		t.Errorf("Result slice has wrong length (%v), should be (%v)", len(exc), num)
	}

	exc, err = Multiple(Max() + 1)
	if err == nil {
		t.Error("Multiple with Max+1 did not return an error")
	}

	exc, err = Multiple(0)
	if err == nil {
		t.Error("Multiple with 0 did not return an error")
	}

	exc, err = Multiple(-1)
	if err == nil {
		t.Error("Multiple with -1 did not return an error")
	}
}

func TestAll(t *testing.T) {
	var exc []string = All()

	if len(exc) < 1 {
		t.Error("No excuses available?")
	}
	if len(exc) != Max() {
		t.Errorf("Amount of excuses returned by All() [%v] does not match Max()[%v]", len(exc), Max())
	}
}

func BenchmarkOne(b *testing.B) {
	for i := 0; i < b.N; i++ {
		One()
	}
}

func BenchmarkMultipleOne(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Multiple(1)
	}
}

func BenchmarkMultipleTwo(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Multiple(2)
	}
}

func BenchmarkMultipleTen(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Multiple(10)
	}
}

func BenchmarkMultipleHundred(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Multiple(100)
	}
}

func BenchmarkMultipleAll(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Multiple(Max())
	}
}

func BenchmarkAll(b *testing.B) {
	for i := 0; i < b.N; i++ {
		All()
	}
}
