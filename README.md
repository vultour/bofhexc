# bofhexc

Bastard Operator From Hell (BOFH) Excuses golang package. Based on Jeff Ballard's generator (http://pages.cs.wisc.edu/~ballard/bofh/)

Documentation: https://godoc.org/gitlab.com/vultour/bofhexc

Don't forget to seed your `rand`.
