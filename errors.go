package bofhexc

import "fmt"

// ErrIndexOOB indicates that Multiple()'s internal logic failed. You should
// probably never see this.
type ErrIndexOOB int

func (e ErrIndexOOB) Error() string {
	return fmt.Sprintf("Index out of bounds: %v", int(e))
}

// ErrRange indicates an error with the requested amount of excuses.
type ErrRange int

func (e ErrRange) Error() string {
	return fmt.Sprintf("Invalid number of excuses (%v), valid range is (0)-(%v)", int(e), Max())
}
