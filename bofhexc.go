// Package bofhexc provides access to Jeff Ballard's Bastard Operator From Hell
// Excuses (http://pages.cs.wisc.edu/~ballard/bofh/)
package bofhexc

import (
	"math/rand"
)

func del(s []string, index int) ([]string, string, error) {
	if (index < 0) || (index > (len(s) - 1)) {
		return s, "", ErrIndexOOB(index)
	}

	var output string = s[index]
	s = append(s[:index], s[index+1:]...)

	return s, output, nil
}

// Max returns the total amount of excuses available.
func Max() int {
	return len(excuses)
}

// One returns one random excuse.
func One() string {
	return excuses[rand.Intn(len(excuses))]
}

// Multiple returns the requested amount of excuses.
// If n is less than 1 or more than the available amount of excuses (Max())
// then an error is returned.
func Multiple(n int) ([]string, error) {
	if (n > len(excuses)) || (n < 1) {
		return []string{}, ErrRange(n)
	}

	exc := make([]string, len(excuses))
	copy(exc, excuses)
	output := make([]string, 0, n)
	for i := 0; i < n; i++ {
		var excuse string
		var err error
		exc, excuse, err = del(exc, rand.Intn(len(exc)))
		if err != nil {
			return []string{}, err
		}
		output = append(output, excuse)
	}
	return output, nil
}

// All returns a slice containing all available excuses.
func All() []string {
	return excuses[:]
}
